package challenges.flow;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import flow.Engine;
import flow.IAdapter;
import flow.IAgent;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Map;

public class MyStepdefs {

        private IAgent user;
        private Map<String, IAdapter> adapters;
        private EmailApp app;
        private Engine engine;

        @Given("^A User sends an \"([^\"]*)\" message$")
        public void a_User_sends_an_message(String msgSent) throws Throwable {
                this.user = new UserAgent(msgSent);
                this.adapters = new HashMap<String, IAdapter>();
                this.adapters.put("EMAIL", new EmailAdapter());
        }
        
        @When("^The message is converted by the Adapter$")
        public void the_message_is_converted_by_the_Adapter() throws Throwable {
                this.app = new EmailApp();
                this.engine = new Engine(user, adapters, app);
                this.engine.run();
        }

        @Then("^The Message server should contain the \"([^\"]*)\" message in the queue$")
        public void the_Message_server_should_contain_the_message_in_the_queue(String msgReceived) throws Throwable {
                Assert.assertEquals(msgReceived, this.app.popMessage());
        }

}