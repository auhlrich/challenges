package challenges.flow;

import flow.ActionException;
import flow.EventException;
import flow.IEvent;

public class EmailEvent implements IEvent<String> {
    private String value;

    public EmailEvent(String value) {
        this.value = value;
    }

    public String trigger() throws EventException, ActionException {
        return "EMAIL:" + value;
    }
}
