package challenges.flow;

import flow.ActionException;
import flow.AdapterException;
import flow.IAdapter;
import flow.IEvent;

public class EmailAdapter implements IAdapter<EmailAction> {

    public IEvent adapt(EmailAction action) throws AdapterException, ActionException {
        String value = action.execute();
        return new EmailEvent(value);
    }
}