package challenges.flow;

import flow.AgentException;
import flow.IAction;
import flow.IAgent;

import java.util.LinkedList;
import java.util.List;

public class UserAgent implements IAgent {
    private String[] values;

    public UserAgent() {
        this.values = new String[1];
        this.values[0] = "hello";
    }

    public UserAgent(String... values) {
        this.values = values;
    }

    public List<IAction> act() throws AgentException {
        final List<IAction> flow = new LinkedList<IAction>();
        for(final String value : values) {
            flow.add(new EmailAction(value));
        }
        return flow;
    }
}
